from tomcat
maintainer jarroddocker

copy maven-app/target/maven-app.war /usr/local/tomcat/webapps/maven-app.war

expose 8080

CMD ["catalina.sh", "run"]
