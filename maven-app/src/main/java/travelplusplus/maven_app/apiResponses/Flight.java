package travelplusplus.maven_app.apiResponses;


import com.fasterxml.jackson.annotation.JsonAutoDetect;

import travelplusplus.maven_app.apiResponses.FlightModels.FlightInboundSegment;
import travelplusplus.maven_app.apiResponses.FlightModels.FlightOutboundSegment;

import javax.ws.rs.core.Response;
import java.util.List;

@JsonAutoDetect
public class Flight {

    public int resultID;
    public String fareTotalPrice;
    public String farePricePerChild;
    public String farePricePerChildTax;
    public String farePricePerAdult;
    public String farePricePerAdultTax;
    public String fareCurrency;
    public Integer outboundSeatsRemaining;
    public Integer inboundSeatsRemaining;
    public String travelClass;
    public Response.Status status;

    public List<FlightOutboundSegment> flightOutboundSegmentList;
    public List<FlightInboundSegment> flightInboundSegmentList;

    public String fromAirport;
    public String toAirport;

    //For the brief
    public String outboundFlightTime;
    public String outboundFlightTimeDuration;
    public String returnFlightTime;
    public String returnFlightTimeDuration;
    public String outboundAirlineName;
    public String outboundFromToAirport;
    public String returnAirlineName;
    public String returnFromToAirport;

    public Flight(){

    }

    public int getResultID() {
        return resultID;
    }

    public void setResultID(int resultID) {
        this.resultID = resultID;
    }

    public String getFareTotalPrice() {
        return fareTotalPrice;
    }

    public void setFareTotalPrice(String fareTotalPrice) {
        this.fareTotalPrice = fareTotalPrice;
    }

    public String getFarePricePerChild() {
        return farePricePerChild;
    }

    public void setFarePricePerChild(String farePricePerChild) {
        this.farePricePerChild = farePricePerChild;
    }

    public String getFarePricePerChildTax() {
        return farePricePerChildTax;
    }

    public void setFarePricePerChildTax(String farePricePerChildTax) {
        this.farePricePerChildTax = farePricePerChildTax;
    }

    public String getFarePricePerAdult() {
        return farePricePerAdult;
    }

    public void setFarePricePerAdult(String farePricePerAdult) {
        this.farePricePerAdult = farePricePerAdult;
    }

    public String getFarePricePerAdultTax() {
        return farePricePerAdultTax;
    }

    public void setFarePricePerAdultTax(String farePricePerAdultTax) {
        this.farePricePerAdultTax = farePricePerAdultTax;
    }

    public String getFareCurrency() {
        return fareCurrency;
    }

    public void setFareCurrency(String fareCurrency) {
        this.fareCurrency = fareCurrency;
    }

    public Integer getInboundSeatsRemaining() {
        return inboundSeatsRemaining;
    }

    public void setInboundSeatsRemaining(Integer inboundSeatsRemaining) {
        this.inboundSeatsRemaining = inboundSeatsRemaining;
    }

    public Integer getOutboundSeatsRemaining() {
        return outboundSeatsRemaining;
    }

    public void setOutboundSeatsRemaining(Integer outboundSeatsRemaining) {
        this.outboundSeatsRemaining = outboundSeatsRemaining;
    }

    public String getTravelClass() {
        return travelClass;
    }

    public void setTravelClass(String travelClass) {
        this.travelClass = travelClass;
    }

    public List<FlightOutboundSegment> getFlightOutboundSegmentList() {
        return flightOutboundSegmentList;
    }

    public void setFlightOutboundSegmentList(List<FlightOutboundSegment> flightOutboundSegmentList) {
        this.flightOutboundSegmentList = flightOutboundSegmentList;
    }

    public List<FlightInboundSegment> getFlightInboundSegmentList() {
        return flightInboundSegmentList;
    }

    public void setFlightInboundSegmentList(List<FlightInboundSegment> flightInboundSegmentList) {
        this.flightInboundSegmentList = flightInboundSegmentList;
    }

    public Response.Status getStatus() {
        return status;
    }

    public void setStatus(Response.Status status) {
        this.status = status;
    }

    public String getFromAirport() {
        return fromAirport;
    }

    public void setFromAirport(String fromAirport) {
        this.fromAirport = fromAirport;
    }

    public String getToAirport() {
        return toAirport;
    }

    public void setToAirport(String toAirport) {
        this.toAirport = toAirport;
    }


    public String getOutboundFlightTime() {
        return outboundFlightTime;
    }

    public void setOutboundFlightTime(String outboundFlightTime) {
        this.outboundFlightTime = outboundFlightTime;
    }

    public String getOutboundFlightTimeDuration() {
        return outboundFlightTimeDuration;
    }

    public void setOutboundFlightTimeDuration(String outboundFlightTimeDuration) {
        this.outboundFlightTimeDuration = outboundFlightTimeDuration;
    }

    public String getReturnFlightTime() {
        return returnFlightTime;
    }

    public void setReturnFlightTime(String returnFlightTime) {
        this.returnFlightTime = returnFlightTime;
    }

    public String getReturnFlightTimeDuration() {
        return returnFlightTimeDuration;
    }

    public void setReturnFlightTimeDuration(String returnFlightTimeDuration) {
        this.returnFlightTimeDuration = returnFlightTimeDuration;
    }

    public String getOutboundAirlineName() {
        return outboundAirlineName;
    }

    public void setOutboundAirlineName(String outboundAirlineName) {
        this.outboundAirlineName = outboundAirlineName;
    }

    public String getOutboundFromToAirport() {
        return outboundFromToAirport;
    }

    public void setOutboundFromToAirport(String outboundFromToAirport) {
        this.outboundFromToAirport = outboundFromToAirport;
    }

    public String getReturnAirlineName() {
        return returnAirlineName;
    }

    public void setReturnAirlineName(String returnAirlineName) {
        this.returnAirlineName = returnAirlineName;
    }

    public String getReturnFromToAirport() {
        return returnFromToAirport;
    }

    public void setReturnFromToAirport(String returnFromToAirport) {
        this.returnFromToAirport = returnFromToAirport;
    }
}
