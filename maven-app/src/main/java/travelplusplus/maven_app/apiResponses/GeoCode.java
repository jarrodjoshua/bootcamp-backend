package travelplusplus.maven_app.apiResponses;


public class GeoCode {

    private String localName;
    private String administrativeAreaLevel2;
    private String getAdministrativeAreaLevel1;
    private String country;

    private String latitude;
    private String longitude;

    public GeoCode(){

    }


    public String getLocalName() {
        return localName;
    }

    public void setLocalName(String localName) {
        this.localName = localName;
    }

    public String getAdministrativeAreaLevel2() {
        return administrativeAreaLevel2;
    }

    public void setAdministrativeAreaLevel2(String administrativeAreaLevel2) {
        this.administrativeAreaLevel2 = administrativeAreaLevel2;
    }

    public String getGetAdministrativeAreaLevel1() {
        return getAdministrativeAreaLevel1;
    }

    public void setGetAdministrativeAreaLevel1(String getAdministrativeAreaLevel1) {
        this.getAdministrativeAreaLevel1 = getAdministrativeAreaLevel1;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }


}
