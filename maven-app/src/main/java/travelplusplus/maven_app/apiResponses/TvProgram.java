package travelplusplus.maven_app.apiResponses;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonAutoDetect
public class TvProgram implements Serializable {

    private String id;
    private String title;
    private String type;
    private String imageUrl;
    private String city;
    private String lat;
    private String lon;
    private double rating;

    public TvProgram(String id, String title, String type, String imageUrl, String city, double rating) {
        this.id = id;
        this.title = title;
        this.type = type;
        this.imageUrl = imageUrl;
        this.city = city;
        this.rating = rating;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    @JsonProperty
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @JsonProperty
    public String getCity() {
        return city;
    }

    public void setCity(String associatedCity) {
        this.city = associatedCity;
    }
}
