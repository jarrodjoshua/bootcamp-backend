package travelplusplus.maven_app.dao;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
 
public class HibernateUtil {
	
    private static final SessionFactory sessionFactory = buildSessionFactory();
    private static Session sesh;
 
    private static SessionFactory buildSessionFactory() {
        try {
        	
        	// SSH in and THEN build the session
        	JSch jsch = new JSch();
			String sshUser = "ec2-user";
			String sshHost = "ec2-34-252-123-11.eu-west-1.compute.amazonaws.com";
			String sshPem = getFilePathString("sql-dbs.pem");
			jsch.addIdentity(sshPem);
			JSch.setConfig("StrictHostKeyChecking", "no");
			System.out.println("JSch set up");
			Session sesh = jsch.getSession(sshUser, sshHost, 22);
			System.out.println(".getSession called");
			sesh.connect();
			System.out.println("Connected");
			//by security policy, you must connect through a forwarded port          
	        sesh.setPortForwardingL(3307, "127.0.0.1", 3306); 
        	//Testpush for CI Server
            // Create the SessionFactory from hibernate.cfg.xml
	        System.out.println("Session factory");

            return new Configuration().configure().buildSessionFactory();
            
			//  https://stackoverflow.com/questions/26860762/java-lang-noclassdeffounderror-could-not-initialize-class-xxx-xxx-xxx-hibernate
            
        } catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
 
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
 
    public static void shutdown() {
        // Close caches and connection pools
        getSessionFactory().close();
		sesh.disconnect();
		//System.out.println("off");
    }
    
    
    private static String getFilePathString(String fileName) {
    	String path;
		// Get file from resources folder
		ClassLoader classLoader = HibernateUtil.class.getClassLoader();
		path = classLoader.getResource(fileName).getFile();
		return path;
	}
 
}