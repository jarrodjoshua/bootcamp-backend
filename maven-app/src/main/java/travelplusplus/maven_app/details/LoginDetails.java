package travelplusplus.maven_app.details;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class LoginDetails {

    private String username;
    private String password;


    public LoginDetails(){

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
