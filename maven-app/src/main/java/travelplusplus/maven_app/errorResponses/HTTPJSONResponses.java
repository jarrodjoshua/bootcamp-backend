package travelplusplus.maven_app.errorResponses;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class HTTPJSONResponses {

    private String message;
    private Response.Status status;

    public void setMessage(String message) {
        this.message = message;
    }

    public void setStatus(Response.Status status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public Response.Status getStatus() {
        return status;
    }

    public static Response notFoundResponse(String message) {

        HTTPJSONResponses notFound = new HTTPJSONResponses();
        notFound.setMessage(message);
        notFound.setStatus(Response.Status.NOT_FOUND);

        return Response.ok(notFound).type(MediaType.APPLICATION_JSON).build();
    }


    public static Response unauthorized(String message) {

        HTTPJSONResponses notFound = new HTTPJSONResponses();
        notFound.setMessage(message);
        notFound.setStatus(Response.Status.UNAUTHORIZED);

        return Response.ok(notFound).type(MediaType.APPLICATION_JSON).build();
    }

    public static Response loginSuccess(String message) {

        HTTPJSONResponses notFound = new HTTPJSONResponses();
        notFound.setMessage(message);
        notFound.setStatus(Response.Status.OK);

        return Response.ok(notFound).type(MediaType.APPLICATION_JSON).build();
    }

}
