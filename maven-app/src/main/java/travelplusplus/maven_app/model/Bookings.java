package travelplusplus.maven_app.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "bookings")
@JsonAutoDetect
public class Bookings {

	@Id
	private Integer bookingid;

	@ManyToOne
	@JoinColumn(name = "username")
	@JsonBackReference
	private User user;

	private String flightTimeDep;
	private String flightTimeArrival;
	private String flightClass;
	private String flightNoAdults;
	private String flightNoChildren;
	private String flightPrice;
	private String flightFromAirport;
	private String flightToAirport;
	private String flightAircraft;
	private String hotelName;
	private String hotelLocation;
	private String hotelRating;
	private String hotelTimeCheckIn;
	private String hoteTimeCheckOut;
	private String hoteltNoAdults;
	private String hotelNoChildren;
	private String hotelBedType;


	public Bookings() {

	}


	public Integer getBookingid() {
		return bookingid;
	}

	public void setBookingid(Integer bookingid) {
		this.bookingid = bookingid;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getFlightTimeDep() {
		return flightTimeDep;
	}

	public void setFlightTimeDep(String flightTimeDep) {
		this.flightTimeDep = flightTimeDep;
	}

	public String getFlightTimeArrival() {
		return flightTimeArrival;
	}

	public void setFlightTimeArrival(String flightTimeArrival) {
		this.flightTimeArrival = flightTimeArrival;
	}

	public String getFlightClass() {
		return flightClass;
	}

	public void setFlightClass(String flightClass) {
		this.flightClass = flightClass;
	}

	public String getFlightNoAdults() {
		return flightNoAdults;
	}

	public void setFlightNoAdults(String flightNoAdults) {
		this.flightNoAdults = flightNoAdults;
	}

	public String getFlightNoChildren() {
		return flightNoChildren;
	}

	public void setFlightNoChildren(String flightNoChildren) {
		this.flightNoChildren = flightNoChildren;
	}

	public String getFlightPrice() {
		return flightPrice;
	}

	public void setFlightPrice(String flightPrice) {
		this.flightPrice = flightPrice;
	}

	public String getFlightFromAirport() {
		return flightFromAirport;
	}

	public void setFlightFromAirport(String flightFromAirport) {
		this.flightFromAirport = flightFromAirport;
	}

	public String getFlightToAirport() {
		return flightToAirport;
	}

	public void setFlightToAirport(String flightToAirport) {
		this.flightToAirport = flightToAirport;
	}

	public String getFlightAircraft() {
		return flightAircraft;
	}

	public void setFlightAircraft(String flightAircraft) {
		this.flightAircraft = flightAircraft;
	}

	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public String getHotelLocation() {
		return hotelLocation;
	}

	public void setHotelLocation(String hotelLocation) {
		this.hotelLocation = hotelLocation;
	}

	public String getHotelRating() {
		return hotelRating;
	}

	public void setHotelRating(String hotelRating) {
		this.hotelRating = hotelRating;
	}

	public String getHotelTimeCheckIn() {
		return hotelTimeCheckIn;
	}

	public void setHotelTimeCheckIn(String hotelTimeCheckIn) {
		this.hotelTimeCheckIn = hotelTimeCheckIn;
	}

	public String getHoteTimeCheckOut() {
		return hoteTimeCheckOut;
	}

	public void setHoteTimeCheckOut(String hoteTimeCheckOut) {
		this.hoteTimeCheckOut = hoteTimeCheckOut;
	}

	public String getHoteltNoAdults() {
		return hoteltNoAdults;
	}

	public void setHoteltNoAdults(String hoteltNoAdults) {
		this.hoteltNoAdults = hoteltNoAdults;
	}

	public String getHotelNoChildren() {
		return hotelNoChildren;
	}

	public void setHotelNoChildren(String hotelNoChildren) {
		this.hotelNoChildren = hotelNoChildren;
	}

	public String getHotelBedType() {
		return hotelBedType;
	}

	public void setHotelBedType(String hotelBedType) {
		this.hotelBedType = hotelBedType;
	}
}
