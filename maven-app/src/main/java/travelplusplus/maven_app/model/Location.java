package travelplusplus.maven_app.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import javax.persistence.*;

@Entity
@Table(name = "location")
@JsonAutoDetect
public class Location {

    @Id
    private Integer locationid;
    private String nameOfPlace;

    private String longitude;
    private String latitutde;

    public Location() {

    }

    public Integer getLocationid() {
        return locationid;
    }

    public void setLocationid(Integer locationid) {
        this.locationid = locationid;
    }

    public String getNameOfPlace() {
        return nameOfPlace;
    }

    public void setNameOfPlace(String nameOfPlace) {
        this.nameOfPlace = nameOfPlace;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitutde() {
        return latitutde;
    }

    public void setLatitutde(String latitutde) {
        this.latitutde = latitutde;
    }
}
