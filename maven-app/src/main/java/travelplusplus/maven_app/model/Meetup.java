package travelplusplus.maven_app.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "meetup")
@JsonAutoDetect
public class Meetup {

	@Id
	private Integer meetid;

	@ManyToOne
	@JoinColumn(name = "username")
	@JsonBackReference
	private User user;

	private String currentLong;
	private String currentLatitutde;
	private String withWho;
	private String plans;
	private String age;

	public Meetup() {

	}


	public Integer getMeetid() {
		return meetid;
	}

	public void setMeetid(Integer meetid) {
		this.meetid = meetid;
	}

	public User getUsername() {
		return user;
	}

	public void setUsername(User username) {
		this.user = username;
	}

	public String getCurrentLong() {
		return currentLong;
	}

	public void setCurrentLong(String currentLong) {
		this.currentLong = currentLong;
	}

	public String getCurrentLatitutde() {
		return currentLatitutde;
	}

	public void setCurrentLatitutde(String currentLatitutde) {
		this.currentLatitutde = currentLatitutde;
	}

	public String getWithWho() {
		return withWho;
	}

	public void setWithWho(String withWho) {
		this.withWho = withWho;
	}

	public String getPlans() {
		return plans;
	}

	public void setPlans(String plans) {
		this.plans = plans;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}
}
