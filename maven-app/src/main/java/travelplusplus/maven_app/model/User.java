package travelplusplus.maven_app.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "users")
@JsonAutoDetect
public class User {

	@Id
	private String username;
	private String first_name;
	private String last_name;
	private String password;
	private Integer socialMediaEnabled;
	private String rank;
	private String role;


	@OneToMany(targetEntity=Trip.class, mappedBy="user")
	private List<Trip> trips;


	@OneToMany(targetEntity=Bookings.class, mappedBy="user")
	private List<Bookings> bookings;


	@OneToMany(targetEntity=Meetup.class, mappedBy="user")
	@JsonBackReference
	private List<Meetup> meetups;


	public User() {

	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getSocialMediaEnabled() {
		return socialMediaEnabled;
	}

	public void setSocialMediaEnabled(Integer socialMediaEnabled) {
		this.socialMediaEnabled = socialMediaEnabled;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public List<Trip> getTrips() {
		return trips;
	}

	public void setTrips(List<Trip> trips) {
		this.trips = trips;
	}

	public List<Bookings> getBookings() {
		return bookings;
	}

	public void setBookings(List<Bookings> bookings) {
		this.bookings = bookings;
	}

	public List<Meetup> getMeetups() {
		return meetups;
	}

	public void setMeetups(List<Meetup> meetups) {
		this.meetups = meetups;
	}
}
