package travelplusplus.maven_app.resources;

import travelplusplus.maven_app.apiResponses.Flight;
import travelplusplus.maven_app.apiResponses.FlightModels.FlightInboundSegment;
import travelplusplus.maven_app.apiResponses.FlightModels.FlightOutboundSegment;

import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static travelplusplus.maven_app.errorResponses.HTTPJSONResponses.notFoundResponse;
import static travelplusplus.maven_app.utilities.FlightUtil.getFlightAPIInfo;
import static travelplusplus.maven_app.utilities.FlightUtil.getIATA_AirlineName;
import static travelplusplus.maven_app.utilities.FlightUtil.getIATA_AirportName;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("flight")
public class FlightResource {

    /**
     * This API endpoint utilises the "AMADEUS Affiliate Search API" found in
     * https://sandbox.amadeus.com/travel-innovation-sandbox/apis/get/flights/affiliate-search
     *
     * API KEY: *SEE FlightUtil.java*
     *
     * @param originAirport      - LHR
     * @param destinationAirport - LAX
     * @param departureDate      - 2018-10-07
     * @param returnDate         - 2018-10-09
     * @param noOfAdults         - 2
     * @param noOfChildren       - 2
     */
    @GET
    public Response getFlightInformation (
            @QueryParam("originAirport") String originAirport,
            @QueryParam("destinationAirport") String destinationAirport,
            @QueryParam("departureDate") String departureDate,
            @QueryParam("returnDate") String returnDate,
            @QueryParam("noOfAdults") Integer noOfAdults,
            @QueryParam("noOfChildren") Integer noOfChildren

    ) throws URISyntaxException, IOException, ParseException {

        List<Flight> flightList = new ArrayList<>();

        JSONArray allResults = new JSONObject(getFlightAPIInfo(
                originAirport,
                destinationAirport,
                departureDate,
                returnDate,
                noOfAdults,
                noOfChildren
        )).getJSONArray("results");

        if (allResults.length() == 0){
            return notFoundResponse("No results were found with " + originAirport + " -> " + destinationAirport);
        }

        String fromAirportName = getIATA_AirportName(originAirport);
        String toAirportName = getIATA_AirportName(destinationAirport);


        for (int i = 0; i < 7; i++) {
            Flight flight = new Flight();
            flight.setResultID(i + 1);

            flight.setFromAirport(fromAirportName);
            flight.setToAirport(toAirportName);

            List<FlightOutboundSegment> flightOutboundSegmentList = new ArrayList<>();
            List<FlightInboundSegment> flightInboundSegmentList = new ArrayList<>();

            JSONArray outbounds = allResults.getJSONObject(i).getJSONObject("outbound").getJSONArray("flights");
            JSONArray inbounds = allResults.getJSONObject(i).getJSONObject("inbound").getJSONArray("flights");


            for (int z = 0; z < outbounds.length(); z++) {
                JSONObject jsonObject = outbounds.getJSONObject(z);

                FlightOutboundSegment flightOutboundSegment = new FlightOutboundSegment();

                flightOutboundSegment.setSegmentNumber(z+1);

                flightOutboundSegment.setDepartureAirport(jsonObject.getJSONObject("origin").getString("airport"));
                flightOutboundSegment.setArrivalAirport(jsonObject.getJSONObject("destination").getString("airport"));
                flightOutboundSegment.setDepartureDateTime(jsonObject.getString("departs_at"));
                flightOutboundSegment.setArrivalDateTime(jsonObject.getString("arrives_at"));
                flightOutboundSegment.setFlightNumber(Integer.parseInt(jsonObject.getString("flight_number")));
                flightOutboundSegment.setAircraft(jsonObject.getString("aircraft"));
                flightOutboundSegment.setMarketingAirline(jsonObject.getString("marketing_airline"));

                flightOutboundSegment.setDepartureAirportName(getIATA_AirportName(jsonObject.getJSONObject("origin").getString("airport")));
                flightOutboundSegment.setArrivalAirportName(getIATA_AirportName(jsonObject.getJSONObject("destination").getString("airport")));

                if (!jsonObject.getJSONObject("destination").isNull("terminal")) {
                    flightOutboundSegment.setDestinationTerminal(jsonObject.getJSONObject("destination").getString("terminal"));
                } else {
                    flightOutboundSegment.setDestinationTerminal("No terminal: flight terminates here");
                }

                flightOutboundSegmentList.add(flightOutboundSegment);
            }

            for (int y = 0; y < inbounds.length(); y++) {
                JSONObject jsonObject = inbounds.getJSONObject(y);

                FlightInboundSegment flightInboundSegment = new FlightInboundSegment();

                flightInboundSegment.setSegmentNumber(y+1);

                flightInboundSegment.setDepartureAirport(jsonObject.getJSONObject("origin").getString("airport"));
                flightInboundSegment.setArrivalAirport(jsonObject.getJSONObject("destination").getString("airport"));
                flightInboundSegment.setDepartureDateTime(jsonObject.getString("departs_at"));
                flightInboundSegment.setArrivalDateTime(jsonObject.getString("arrives_at"));
                flightInboundSegment.setFlightNumber(Integer.parseInt(jsonObject.getString("flight_number")));
                flightInboundSegment.setAircraft(jsonObject.getString("aircraft"));
                flightInboundSegment.setMarketingAirline(jsonObject.getString("marketing_airline"));

                if (!jsonObject.getJSONObject("destination").isNull("terminal")) {
                    flightInboundSegment.setDestinationTerminal(jsonObject.getJSONObject("destination").getString("terminal"));
                } else {
                    flightInboundSegment.setDestinationTerminal("No terminal: flight terminates here");
                }

                flightInboundSegmentList.add(flightInboundSegment);
            }

            //Fare
            flight.setFareTotalPrice(allResults.getJSONObject(i).getJSONObject("fare").getString("total_price"));
            flight.setFarePricePerChild(allResults.getJSONObject(i).getJSONObject("fare").getJSONObject("price_per_child").getString("total_fare"));
            flight.setFarePricePerChildTax(allResults.getJSONObject(i).getJSONObject("fare").getJSONObject("price_per_child").getString("tax"));
            flight.setFarePricePerAdult(allResults.getJSONObject(i).getJSONObject("fare").getJSONObject("price_per_adult").getString("total_fare"));
            flight.setFarePricePerAdultTax(allResults.getJSONObject(i).getJSONObject("fare").getJSONObject("price_per_child").getString("tax"));
            flight.setFareCurrency(allResults.getJSONObject(i).getJSONObject("fare").getString("currency"));

            //Inbound
            flight.setInboundSeatsRemaining(allResults.getJSONObject(i).getJSONObject("inbound").getJSONArray("flights").getJSONObject(0).getJSONObject("booking_info").getInt("seats_remaining"));

            //Outbound
            flight.setOutboundSeatsRemaining(allResults.getJSONObject(i).getJSONObject("outbound").getJSONArray("flights").getJSONObject(0).getJSONObject("booking_info").getInt("seats_remaining"));

            //Travel Class
            flight.setTravelClass(allResults.getJSONObject(i).getString("travel_class"));


            flight.setFlightOutboundSegmentList(flightOutboundSegmentList);
            flight.setFlightInboundSegmentList(flightInboundSegmentList);

            flight.setStatus(Response.Status.OK);



            //For the extra filtered information for the briefFlightContainer
            //Get the first outbounds object

            JSONObject firstOutbound = outbounds.getJSONObject(0);
            JSONObject lastOutbound = outbounds.getJSONObject(outbounds.length() - 1);

            String outboundDepartsAt = (firstOutbound.getString("departs_at")).substring(firstOutbound.getString("departs_at").length() - 5, firstOutbound.getString("departs_at").length());
            String outboundArrivesAt = (firstOutbound.getString("arrives_at")).substring(firstOutbound.getString("arrives_at").length() - 5, firstOutbound.getString("arrives_at").length());

            flight.setOutboundFlightTime(outboundDepartsAt + " - " + outboundArrivesAt);
            flight.setOutboundFlightTimeDuration(hourConverter(outboundDepartsAt, outboundArrivesAt));

            String returnDepartsAt = (lastOutbound.getString("departs_at")).substring(lastOutbound.getString("departs_at").length() - 5, lastOutbound.getString("departs_at").length());
            String returnArrivesAt = (lastOutbound.getString("arrives_at")).substring(lastOutbound.getString("arrives_at").length() - 5, lastOutbound.getString("arrives_at").length());

            flight.setReturnFlightTime(returnDepartsAt + " - " + returnArrivesAt);
            flight.setReturnFlightTimeDuration(hourConverter(returnDepartsAt, returnArrivesAt));

            flight.setOutboundAirlineName(getIATA_AirlineName(firstOutbound.getString("operating_airline")));
            flight.setReturnAirlineName(getIATA_AirlineName(lastOutbound.getString("operating_airline")));

            flight.setOutboundFromToAirport(firstOutbound.getJSONObject("origin").getString("airport") + " - " + lastOutbound.getJSONObject("origin").getString("airport"));
            flight.setReturnFromToAirport(lastOutbound.getJSONObject("origin").getString("airport") + " - " + firstOutbound.getJSONObject("origin").getString("airport"));

            flightList.add(flight);

        }

        return Response.ok(flightList).type(MediaType.APPLICATION_JSON).build();

    }

    private String hourConverter(String time1, String time2) throws ParseException {
        DateFormat df = new java.text.SimpleDateFormat("hh:mm");
        Date date1 = df.parse(time1);
        Date date2 = df.parse(time2);

        long diff = date2.getTime() - date1.getTime();

        int timeInSeconds = (int) (diff / 1000);
        int hours, minutes;
        hours = timeInSeconds / 3600;
        timeInSeconds = timeInSeconds - (hours * 3600);
        minutes = timeInSeconds / 60;
        timeInSeconds = timeInSeconds - (minutes * 60);


        return String.valueOf(hours + "h " + minutes + "m");
    }


}
