package travelplusplus.maven_app.resources;

import travelplusplus.maven_app.apiResponses.GeoCode;

import org.json.JSONArray;
import org.json.JSONObject;
import travelplusplus.maven_app.utilities.GeoCodeUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URISyntaxException;

@Path("geocode")
public class GeoCodeResource {

    @GET
    public Response getLocationCoords(@QueryParam("place") String place)
            throws URISyntaxException, IOException {

        String filterString = place.replaceAll("\\s","");

        JSONObject object = new JSONObject(GeoCodeUtil.getInstance.getLocationCoordsUsingPlace(filterString));
        JSONArray resultsKey = object.getJSONArray("results");
        JSONObject formattedAddressKey = resultsKey.getJSONObject(0);
        JSONArray formatted_address = formattedAddressKey.getJSONArray("address_components");

        //Names
        String localName = formatted_address.getJSONObject(0).getString("long_name");
        String administrativeAreaLevel2 = formatted_address.getJSONObject(1).getString("long_name");
        String administrativeAreaLevel1 = formatted_address.getJSONObject(2).getString("long_name");
        String country = formatted_address.getJSONObject(3).getString("long_name");

        //Coords
        Double longitude = resultsKey.getJSONObject(0).getJSONObject("geometry").getJSONObject("location").getDouble("lng");
        Double latitude = resultsKey.getJSONObject(0).getJSONObject("geometry").getJSONObject("location").getDouble("lat");

        GeoCode geoCode = new GeoCode();
        geoCode.setLocalName(localName);
        geoCode.setAdministrativeAreaLevel2(administrativeAreaLevel2);
        geoCode.setGetAdministrativeAreaLevel1(administrativeAreaLevel1);
        geoCode.setCountry(country);
        geoCode.setLongitude(String.valueOf(longitude));
        geoCode.setLatitude(String.valueOf(latitude));

		return Response.ok(geoCode).type(MediaType.APPLICATION_JSON).build();
    	
    }


    
}
