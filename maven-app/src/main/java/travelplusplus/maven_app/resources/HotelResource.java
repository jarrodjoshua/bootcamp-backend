package travelplusplus.maven_app.resources;

import org.json.JSONArray;
import org.json.JSONObject;

import travelplusplus.maven_app.apiResponses.Hotel;
import travelplusplus.maven_app.utilities.HotelUtil;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static travelplusplus.maven_app.errorResponses.HTTPJSONResponses.notFoundResponse;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@Path("hotel")
public class HotelResource {

    /**
     * This API endpoint utilises the "AMADEUS Hotel GeoSearch by Circle" found in
     * https://sandbox.amadeus.com/travel-innovation-sandbox/apis/get/hotels/search-circle
     *
     * API KEY: *SEE HotelUtil.java*
     *
     * @param latitude           - 39.6953
     * @param longitude          - 3.0176
     * @param radius             - 42
     * @param checkIn            - 2018-10-07
     * @param checkOut           - 2018-10-09
     */
    @GET
    public Response getHotelInformation (
            @QueryParam("latitude") String latitude,
            @QueryParam("longitude") String longitude,
            @QueryParam("radius") Integer radius,
            @QueryParam("checkIn") String checkIn,
            @QueryParam("checkOut") String checkOut

    ) throws URISyntaxException, IOException {

        List<Hotel> hotelList = new ArrayList<>();

        JSONArray allResults = new JSONObject(HotelUtil.getInstance.getHotelAPIInfo(
                latitude,
                longitude,
                radius,
                checkIn,
                checkOut
        )).getJSONArray("results");

        if (allResults.length() == 0){
            return notFoundResponse("No results were found with " + latitude + " and " + longitude);
        }

        for (int i = 0; i < allResults.length(); i++) {
            JSONObject currentObj = allResults.getJSONObject(i);

            Hotel hotel = new Hotel();
            hotel.setHotelName(currentObj.getString("property_name"));
            hotel.setAddress(
                            currentObj.getJSONObject("address").getString("line1")
                                    + ", " +
                            currentObj.getJSONObject("address").getString("postal_code")
                            );
            hotel.setCountry(currentObj.getJSONObject("address").getString("country"));
            hotel.setCity(currentObj.getJSONObject("address").getString("city"));

            if (currentObj.has("marketing_text")){
                hotel.setMarketingText(currentObj.getString("marketing_text"));
            }

            if (currentObj.getJSONArray("rooms").getJSONObject(0).getJSONObject("room_type_info").has(("bed_type"))) {
                hotel.setBedType(currentObj.getJSONArray("rooms").getJSONObject(0).getJSONObject("room_type_info").getString("bed_type"));
            }

            if (currentObj.getJSONArray("rooms").getJSONObject(0).getJSONObject("room_type_info").has(("number_of_beds"))) {
                hotel.setNumberOfBeds(currentObj.getJSONArray("rooms").getJSONObject(0).getJSONObject("room_type_info").getString("number_of_beds"));
            }


            hotel.setLongitude(String.valueOf(currentObj.getJSONObject("location").getDouble("longitude")));
            hotel.setLatitude(String.valueOf(currentObj.getJSONObject("location").getDouble("latitude")));
            hotel.setPhoneNo(currentObj.getJSONArray("contacts").getJSONObject(0).getString("detail"));


            List<String> amenities = new ArrayList<>();
            for (int z = 0; z < currentObj.getJSONArray("amenities").length(); z++){
                amenities.add((currentObj.getJSONArray("amenities").getJSONObject(z).getString("amenity")));
            }
            hotel.setAmenities(amenities);
            hotel.setTotalPrice(currentObj.getJSONObject("total_price").getString("amount"));

            hotelList.add(hotel);

        }

        return Response.ok(hotelList).type(MediaType.APPLICATION_JSON).build();

    }




}
