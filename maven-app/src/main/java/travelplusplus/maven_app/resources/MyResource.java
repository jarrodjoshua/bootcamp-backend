package travelplusplus.maven_app.resources;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.http.client.ClientProtocolException;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import travelplusplus.maven_app.apiResponses.Weather;

import org.json.JSONObject;
import travelplusplus.maven_app.utilities.HttpRequestUtil;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("myresource")
public class MyResource {

    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt() {
        return "Got it! Pipeline";
    }
    
    @POST
    @Path("weather")
    public Response getWeather(String input) throws URISyntaxException, IOException {
    	HttpRequestUtil util = new HttpRequestUtil();

    	Gson gson = new Gson();
		JsonParser parser = new JsonParser();
		JsonObject jsonInput = parser.parse(input.toString()).getAsJsonObject();
		String lon = jsonInput.get("lon").getAsString();
		String lat = jsonInput.get("lat").getAsString();
    	Response res = util.getWeather(lat, lon);


        JSONObject weatherJSONObj = new JSONObject(res.getEntity().toString());

        Double tempDouble = ((weatherJSONObj.getJSONObject("main").getDouble("temp")) - 273.15);
        int tempInt = tempDouble.intValue();

        Weather weather = new Weather();
        weather.setTemperature(tempInt);
        weather.setCountryCode(weatherJSONObj.getJSONObject("sys").getString("country"));
        weather.setWeather(weatherJSONObj.getJSONArray("weather").getJSONObject(0).getString("main"));
        weather.setWeatherDescription(weatherJSONObj.getJSONArray("weather").getJSONObject(0).getString("description"));

		return Response.ok(weather).type(MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("currency")
    public Response getCurrencyCode(@QueryParam("countryFrom") String countryFrom, 
    		@QueryParam("countryTo") String countryTo) throws ClientProtocolException, URISyntaxException, IOException {
    	HttpRequestUtil util = new HttpRequestUtil();
    	String code1 = util.getCurrencyCodeFromCountryName(countryFrom);
    	String code2 = util.getCurrencyCodeFromCountryName(countryTo);
    	Response output = util.getCurrencyConversionRatesFromCountryCodes(code1, code2);
    	
    	return output;
    }
    
    
}
