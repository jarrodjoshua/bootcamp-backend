package travelplusplus.maven_app.resources;

import static travelplusplus.maven_app.errorResponses.HTTPJSONResponses.notFoundResponse;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONObject;
import travelplusplus.maven_app.utilities.PublicTransportUtil;


@Path("public-transport")
public class PublicTransportResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("train-stations")
    public Response getIt(@QueryParam("lon") String lon, @QueryParam("lat") String lat)
            throws ClientProtocolException, URISyntaxException, IOException {
        // distance in m
        String result = PublicTransportUtil.nearestTrainStations(lat, lon);

        // parse through the result and extract the station JSON objects and put 5 of them
        // into a custom response
        JSONArray allResults = new JSONObject(result).getJSONArray("stations");
        if (allResults.length() == 0) {
            return notFoundResponse("No results were found with " + lat + " , " + lon);
        }
        else {
            return Response.ok(allResults.toString()).build();
        }
    }
}