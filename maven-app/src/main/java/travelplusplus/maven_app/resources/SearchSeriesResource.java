
package travelplusplus.maven_app.resources;

import travelplusplus.maven_app.apiResponses.TvProgram;
import org.json.JSONArray;
import org.json.JSONObject;
import travelplusplus.maven_app.utilities.SearchSeriesUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static travelplusplus.maven_app.errorResponses.HTTPJSONResponses.notFoundResponse;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("search")
public class SearchSeriesResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSearchSuggestions(@QueryParam("query") String query)
            throws URISyntaxException, IOException {


        String suggestions = SearchSeriesUtil.getInstance.getSearchSuggestions(query);

        if (!suggestions.equals("error")) {

            JSONObject object = new JSONObject(suggestions);
            JSONArray jsonArray = object.getJSONArray("results");
            List<TvProgram> programList = new ArrayList<>();

            if (jsonArray.length() > 0) {

                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject program = jsonArray.getJSONObject(i);

                    String id, type, title, imageUrl, city;
                    id = program.getString("uuid");
                    type = program.getString("uuidtype");
                    title = program.getString("t");
                    imageUrl = "http://images.metadata.sky.com/pd-image/" + id;
                    city = "New York";
                    TvProgram tvProgram = new TvProgram(id, title, type, imageUrl, city, getRandomRating());
                    programList.add(tvProgram);

                }

                return Response.ok(programList).type(MediaType.APPLICATION_JSON).allow("OPTIONS").build();

            } else {
                return notFoundResponse("No results were found");
            }

        } else {
            return notFoundResponse("No shows were found");
        }


    }

    public double getRandomRating() {
        return (new java.util.Random().nextDouble() % (5.0 - 3.5)) + 3.5;
    }



}
