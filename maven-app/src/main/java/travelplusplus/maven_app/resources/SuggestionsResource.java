package travelplusplus.maven_app.resources;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import travelplusplus.maven_app.apiResponses.TvProgram;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;


/**
 * Root resource (exposed at "myresource" path)
 */
@Path("suggestions")
public class SuggestionsResource {

    private HttpClient client = HttpClientBuilder.create().build();
    private String latlonUrl = "http://34.255.153.110:8080/maven-app/webapi/geocode?place="
;
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSearchSuggestions()
            throws URISyntaxException, IOException {

        GeoCodeResource geoCodeResource = new GeoCodeResource();

        List<TvProgram> suggestionsList = new ArrayList<>();

        TvProgram loveIsland = new TvProgram("c73294ba-2847-4058-9648-e1c9e0ab8f66",
                "Love Island",
                "series",
                "http://images.metadata.sky.com/pd-image/c73294ba-2847-4058-9648-e1c9e0ab8f66",
                "Majorca", 3.5);

        TvProgram suits = new TvProgram("49e27f96-f64a-41cd-a032-864054589d18",
                "Suits",
                "series",
                "http://images.metadata.sky.com/pd-image/49e27f96-f64a-41cd-a032-864054589d18",
                "New%20York", 4.1);

        TvProgram bigBangtheory = new TvProgram("d21ffe25-3675-4154-8069-6f86721ebd8a",
                "The Big Bang Theory",
                "series",
                "http://images.metadata.sky.com/pd-image/d21ffe25-3675-4154-8069-6f86721ebd8a",
                "Pasadena", 4.5);

        TvProgram breakingBad = new TvProgram("3954623e-8de6-40bf-ac8d-f76a27b5120b",
                "Breaking Bad",
                "series",
                "http://images.metadata.sky.com/pd-image/3954623e-8de6-40bf-ac8d-f76a27b5120b",
                "Albuquerque", 5.0);

        TvProgram gameOfThrones = new TvProgram("e8899cad-639a-482d-8bcd-731c447dfcc8",
                "Game of Thrones",
                "series",
                "http://images.metadata.sky.com/pd-image/e8899cad-639a-482d-8bcd-731c447dfcc8",
                "Dubrovnik", 4.8);

        TvProgram westworld = new TvProgram("7ab848b9-50af-4d7a-98fa-9b7076bbbbcf",
                "Westworld",
                "series",
                "http://images.metadata.sky.com/pd-image/7ab848b9-50af-4d7a-98fa-9b7076bbbbcf",
                "Castle%20Valley", 3.7);


        suggestionsList.add(loveIsland);
        suggestionsList.add(suits);
        suggestionsList.add(bigBangtheory);
        suggestionsList.add(breakingBad);
        suggestionsList.add(gameOfThrones);
        suggestionsList.add(westworld);

        for (TvProgram tvProgram: suggestionsList) {

            HttpGet request = new HttpGet(latlonUrl + tvProgram.getCity());
            HttpResponse response = client.execute(request);
            HttpEntity entity = response.getEntity();
            String result = EntityUtils.toString(entity);

            JSONObject location = new JSONObject(result);
            String lat = location.getString("latitude");
            String lon = location.getString("longitude");

            tvProgram.setLat(lat);
            tvProgram.setLon(lon);

        }

        return Response.ok(suggestionsList).type(MediaType.APPLICATION_JSON).build();
    }


}

