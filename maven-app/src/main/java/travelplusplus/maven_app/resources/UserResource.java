package travelplusplus.maven_app.resources;

import javax.persistence.NoResultException;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.Session;

import org.hibernate.query.NativeQuery;
import org.springframework.web.bind.annotation.CrossOrigin;
import travelplusplus.maven_app.dao.HibernateUtil;
import travelplusplus.maven_app.details.LoginDetails;
import travelplusplus.maven_app.errorResponses.HTTPJSONResponses;
import travelplusplus.maven_app.model.Bookings;
import travelplusplus.maven_app.model.Meetup;
import travelplusplus.maven_app.model.Trip;
import travelplusplus.maven_app.model.User;
import travelplusplus.maven_app.utilities.CryptUtil;

import java.util.ArrayList;
import java.util.List;

@Path("user")
public class UserResource {

    @GET
    @Path("read")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findUsers(@QueryParam("username") String username) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        NativeQuery query = session.createSQLQuery(
                "SELECT * FROM travelplusplus2.users u WHERE u.username = :username")
                .addEntity(User.class)
                .setParameter("username", username);

        session.getTransaction().commit();
        List<User> users = query.list();

        List<User> userList = new ArrayList<>();

        for (int i = 0; i < users.size(); i++){
            User user = users.get(i);
            user.setPassword("-");
            userList.add(user);
        }

        return Response.ok(userList).type(MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("create")
    @Produces(MediaType.APPLICATION_JSON)
    public Response createUser(User user) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        NativeQuery query = session.createSQLQuery(
                "INSERT INTO travelplusplus2.users " +
                        "VALUES (:username, :first_name, :last_name, :password, :socialMediaEnabled, :rank, :role) ");
        query.setParameter("username", user.getUsername());
        query.setParameter("first_name", user.getFirst_name());
        query.setParameter("last_name", user.getLast_name());
        query.setParameter("password", CryptUtil.hashPassword(user.getPassword()));
        query.setParameter("socialMediaEnabled", "0");
        query.setParameter("rank", "-");
        query.setParameter("role", "s");
        query.executeUpdate();


        session.getTransaction().commit();

        return Response.ok(user).type(MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Path("login")
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(LoginDetails loginDetails) {
        LoginDetails details = loginDetails;


        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        NativeQuery query = session.createSQLQuery(
                "SELECT * FROM travelplusplus2.users u WHERE u.username = :username")
                .addEntity(User.class)
                .setParameter("username", details.getUsername());

        session.getTransaction().commit();

        try {
            User user = (User) query.getSingleResult();

            if (CryptUtil.checkPass(loginDetails.getPassword(), user.getPassword()))
                return HTTPJSONResponses.loginSuccess("Login successful");

        } catch (NoResultException e) {
            //System.out.println("username not found");
            return HTTPJSONResponses.unauthorized("The password or username is incorrect");

        }

        return HTTPJSONResponses.unauthorized("The password or username is incorrect");
    }


    @GET
    @Path("trips")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTrips(@QueryParam("username") String username, @QueryParam("isEnabled") Integer isEnabled) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        NativeQuery query;

        if (username.length() == 0){
            query = session.createSQLQuery(
                    "SELECT * FROM travelplusplus2.users u " +
                            "WHERE u.socialMediaEnabled = :socialMediaEnabled ")
                    .setParameter("socialMediaEnabled", isEnabled)
                    .addEntity(User.class);

        } else {
            query = session.createSQLQuery(
                    "SELECT * FROM travelplusplus2.users u WHERE u.username = :username " +
                            "AND u.socialMediaEnabled = :socialMediaEnabled ")
                    .setParameter("username", username)
                    .setParameter("socialMediaEnabled", isEnabled)
                    .addEntity(User.class);
        }


        List result = query.list();
        session.getTransaction().commit();

        return Response.ok(result).type(MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("trips/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllTrips() {

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        NativeQuery query = session.createSQLQuery("SELECT * FROM travelplusplus2.trip t")
                .addEntity(Trip.class);

        List result = query.list();
        session.getTransaction().commit();

        return Response.ok(result).type(MediaType.APPLICATION_JSON).build();
    }


    @GET
    @Path("bookings")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBookings(@QueryParam("username") String username) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        NativeQuery query = session.createSQLQuery(
                "SELECT * FROM bookings b WHERE u.username = :username")
                .setParameter("username", username)
                .addEntity(Bookings.class);

        List result = query.list();
        session.getTransaction().commit();

        return Response.ok(result).type(MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("meetups")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMeetUps(@QueryParam("bookings") String username) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        NativeQuery query = session.createSQLQuery(
                "SELECT * FROM meetup m;")
                .addEntity(Meetup.class);

        List result = query.list();
        session.getTransaction().commit();

        return Response.ok(result).type(MediaType.APPLICATION_JSON).build();
    }
}
