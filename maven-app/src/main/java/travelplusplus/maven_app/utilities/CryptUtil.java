package travelplusplus.maven_app.utilities;

import org.mindrot.jbcrypt.BCrypt;

public class CryptUtil {

    public static String hashPassword(String plainTextPassword){
        return BCrypt.hashpw(plainTextPassword, BCrypt.gensalt());
    }


    public static boolean checkPass(String plainPassword, String hashedPassword) {
        boolean isValid = false;

        if (BCrypt.checkpw(plainPassword, hashedPassword)) {
            isValid = true;
            return isValid;
        }
        return isValid;
    }
}
