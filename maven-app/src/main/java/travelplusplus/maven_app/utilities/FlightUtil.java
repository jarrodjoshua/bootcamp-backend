package travelplusplus.maven_app.utilities;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;

public class FlightUtil {

    public static FlightUtil getInstance = new FlightUtil();

    private static final String AMADEUS_API_KEY = "1my6w2ancLBwKAAuPgcNtrCT2xHRFyor";
    private static final String SABRA_API_KEY = "Bearer T1RLAQIhwVbb+64NmMo5FYgrH7E3Zx4nXZ2LdT7q6ZegFH4FtBDXgGzOdt50LKkKwXDcNqqkAADAOt2O7DoTHJGUdFcVLSkoL1tf/qeqyy/MuG4muxHyOvtmvFzK4WqRbUDuKOX+R05o9UrDM4o3FkGh6f/MAW8u8OALJ4V0yHUs9qDz7tuIN5fqLXp+MCPXe6VErWqu1tMjKJ/oDBwZsml9EeO1Baus/1oXBttPxQ3hkrhBsa3WJEZPLKX1EbFRFvCnMWUbFbTWlOG0alVJrWpz7ZyeY4k4syVgcf7v2GgKGpOWzBl9iXX02G9BIk6nZtAXJNlm5NqU";

    private static HttpClient client = HttpClientBuilder.create().build();


    public static String getFlightAPIInfo (
            String originAirport,
            String destinationAirport,
            String departureDate,
            String returnDate,
            Integer noOfAdults,
            Integer noOfChildren
    )
            throws URISyntaxException, IOException {

        String url =  "https://api.sandbox.amadeus.com/v1.2/flights/affiliate-search?apikey=" + AMADEUS_API_KEY + "&origin="+originAirport+"&destination="+destinationAirport+"&departure_date="+departureDate+"&return_date="+returnDate+"&adults="+noOfAdults+"&children="+noOfChildren+"&max_price=&currency=GBP";

        URIBuilder ub = new URIBuilder(url);
        url = ub.toString();

        HttpGet request = new HttpGet(url);
        HttpResponse response = client.execute(request);

        HttpEntity entity = response.getEntity();
        String result = EntityUtils.toString(entity);

        return result;

    }

    public static String getIATA_AirportName(String airport)
            throws URISyntaxException, IOException {

        String url =  "http://iatacodes.org/api/v6/airports?api_key=c557c582-6011-4308-9958-e02fa67a14b3&code=" + airport;


        URIBuilder ub = new URIBuilder(url);
        url = ub.toString();

        HttpGet request = new HttpGet(url);
        HttpResponse response = client.execute(request);

        HttpEntity entity = response.getEntity();
        String result = EntityUtils.toString(entity);

        JSONObject o = new JSONObject(result);
        String airportCode = o.getJSONArray("response").getJSONObject(0).getString("name");


        return airportCode;

    }


    public static String getIATA_AirlineName(String airlineCode)
            throws URISyntaxException, IOException {

        String url =  "http://iatacodes.org/api/v7/airlines?api_key=c557c582-6011-4308-9958-e02fa67a14b3&iata_code=" + airlineCode;


        URIBuilder ub = new URIBuilder(url);
        url = ub.toString();

        HttpGet request = new HttpGet(url);
        HttpResponse response = client.execute(request);

        HttpEntity entity = response.getEntity();
        String result = EntityUtils.toString(entity);

        JSONObject o = new JSONObject(result);
        String airportName = o.getJSONArray("response").getJSONObject(0).getString("name");


        return airportName;

    }


    /* Old API

    public String getFlightAPIInfo(
            String originAirport,
            String destinationAirport,
            String departureDate,
            String returnDate,
            String filterToNumber,
            String passengerCount
    ) throws URISyntaxException, IOException {

        String url =  "https://api-crt.cert.havail.sabre.com/v1/shop/flights?origin="+originAirport+"&destination="+destinationAirport+"&departuredate="+departureDate+"+&returndate="+returnDate+"&onlineitinerariesonly=N&limit="+filterToNumber+"&offset=1&eticketsonly=N&sortby=totalfare&order=asc&sortby2=departuretime&order2=asc&pointofsalecountry=US&passengercount="+passengerCount;

        URIBuilder ub = new URIBuilder(url);
        url = ub.toString();

        HttpGet request = new HttpGet(url);
        request.addHeader("Authorization", SABRA_API_KEY);
        HttpResponse response = client.execute(request);

        HttpEntity entity = response.getEntity();
        String result = EntityUtils.toString(entity);

        return result;

    }
    */
}
