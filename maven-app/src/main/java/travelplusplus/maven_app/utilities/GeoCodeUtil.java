package travelplusplus.maven_app.utilities;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URISyntaxException;

public class GeoCodeUtil {

    public static GeoCodeUtil getInstance = new GeoCodeUtil();

    private final String GOOGLE_GEO_CODE_API_KEY = "AIzaSyBva8uOSM_OzOHZqcakiscfvIjtHdldF48";

    private HttpClient client = HttpClientBuilder.create().build();

    public String getLocationCoordsUsingPlace(String place)
            throws URISyntaxException, IOException {

        String url =  "https://maps.googleapis.com/maps/api/geocode/json?address=1600+" + place + "&key=" + GOOGLE_GEO_CODE_API_KEY;

        URIBuilder ub = new URIBuilder(url);
        url = ub.toString();

        HttpGet request = new HttpGet(url);
        HttpResponse response = client.execute(request);

        HttpEntity entity = response.getEntity();
        String result = EntityUtils.toString(entity);

        return result;

    }
}
