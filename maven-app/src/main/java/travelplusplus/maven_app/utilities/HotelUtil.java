package travelplusplus.maven_app.utilities;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URISyntaxException;

public class HotelUtil {

    public static HotelUtil getInstance = new HotelUtil();

    private final String AMADEUS_API_KEY = "1my6w2ancLBwKAAuPgcNtrCT2xHRFyor";

    private HttpClient client = HttpClientBuilder.create().build();

    public String getHotelAPIInfo (
            String latitude,
            String longitude,
            Integer radius,
            String checkIn,
            String checkOut
    ) throws URISyntaxException, IOException {

        String url =  "https://api.sandbox.amadeus.com/v1.2/hotels/search-circle?apikey="+AMADEUS_API_KEY+"&latitude="+latitude+"&longitude="+longitude+"&radius="+radius+"&check_in="+checkIn+"&check_out="+checkOut+"&currency=GBP";

        URIBuilder ub = new URIBuilder(url);
        url = ub.toString();

        HttpGet request = new HttpGet(url);
        HttpResponse response = client.execute(request);

        HttpEntity entity = response.getEntity();
        String result = EntityUtils.toString(entity);

        return result;

    }

}
