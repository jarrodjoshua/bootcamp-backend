package travelplusplus.maven_app.utilities;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class HttpRequestUtil {

	private HttpClient client = HttpClientBuilder.create().build();

	public Response getWeather(String lat, String lon) throws URISyntaxException, IOException {

		String url = "http://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}";

		URIBuilder ub = new URIBuilder("http://api.openweathermap.org/data/2.5/weather");
		ub.addParameter("lat", lat);
		ub.addParameter("lon", lon);
		ub.addParameter("APPID", "4f03cb468862b14e6493059fdd454762");
		url = ub.toString();

		HttpGet request = new HttpGet(url);
		HttpResponse response = client.execute(request);
		HttpEntity entity = response.getEntity();
		String result = EntityUtils.toString(entity);

		return Response.status(response.getStatusLine().getStatusCode()).entity(result).type(MediaType.APPLICATION_JSON)
				.build();

	}

	public String getCurrencyCodeFromCountryName(String countryName)
			throws URISyntaxException, ClientProtocolException, IOException {

		String url = "https://restcountries.eu/rest/v2/name/";
		StringBuilder sb = new StringBuilder();
		sb.append(url);
		sb.append(countryName);
		String newUrl = sb.toString();
		URIBuilder ub = new URIBuilder(newUrl);
		url = ub.toString();
		HttpGet request = new HttpGet(url);
		HttpResponse response = client.execute(request);
		HttpEntity entity = response.getEntity();
		String result = EntityUtils.toString(entity);
		JsonParser parser = new JsonParser();
		JsonElement jsonInput = parser.parse(result.toString());
		JsonArray jsonArr = jsonInput.getAsJsonArray();
		JsonObject countryJson = jsonArr.get(0).getAsJsonObject();
		jsonArr = countryJson.get("currencies").getAsJsonArray();
		countryJson = jsonArr.get(0).getAsJsonObject();
		String code = countryJson.get("code").getAsString();
		return code;

	}

	public Response getCurrencyConversionRatesFromCountryCodes(String countryCode1, String countryCode2)
			throws URISyntaxException, ClientProtocolException, IOException {
		StringBuilder sb = new StringBuilder();
		sb.append(countryCode1);
		sb.append("_");
		sb.append(countryCode2);
		String codeParam = sb.toString();
		String url = "http://free.currencyconverterapi.com/api/v5/convert";
		URIBuilder ub = new URIBuilder(url);
		ub.addParameter("q", codeParam);
		ub.addParameter("compact", "y");
		url = ub.toString();
		HttpGet request = new HttpGet(url);
		HttpResponse response = client.execute(request);
		HttpEntity entity = response.getEntity();
		String result = EntityUtils.toString(entity);

		return Response.status(response.getStatusLine().getStatusCode()).entity(result).type(MediaType.APPLICATION_JSON)
				.build();

	}

}
