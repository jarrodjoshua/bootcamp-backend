package travelplusplus.maven_app.utilities;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

public class PublicTransportUtil {

    private static final String appId = "a7ff8650";
    private static final String appKey = "a683ab348340ec9354d7be861f7b8aef";

    private static HttpClient client = HttpClientBuilder.create().build();

    public static String nearestTrainStations(String lat, String lon) throws URISyntaxException, ClientProtocolException, IOException {

        // https://transportapi.com/v3/uk/train/stations/near.json
        // ?app_id=a7ff8650&app_key=a683ab348340ec9354d7be861f7b8aef&lat=51.4728&lon=-0.4876

        String url = "https://transportapi.com/v3/uk/train/stations/near.json";
        URIBuilder ub = new URIBuilder(url);
        ub.addParameter("app_id", appId);
        ub.addParameter("app_key", appKey);
        ub.addParameter("lat", lat);
        ub.addParameter("lon", lon);

        url = ub.toString();

        HttpGet request = new HttpGet(url);
        HttpResponse response = client.execute(request);

        HttpEntity entity = response.getEntity();
        String result = EntityUtils.toString(entity);

        return result;
    }

}