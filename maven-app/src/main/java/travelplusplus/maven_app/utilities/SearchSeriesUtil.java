
package travelplusplus.maven_app.utilities;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URISyntaxException;

public class SearchSeriesUtil {

    public static SearchSeriesUtil getInstance = new SearchSeriesUtil();

    private HttpClient client = HttpClientBuilder.create().build();

    public String getSearchSuggestions(String query)
            throws URISyntaxException, IOException {

        String result;

        String skyTvGuideEndpoint = "http://entity.search.sky.com/suggest/v1/search/test/4101/1/userid?term=" + query;

        URIBuilder ub = new URIBuilder(skyTvGuideEndpoint);
        String url = ub.toString();

        HttpGet request = new HttpGet(url);
        HttpResponse response = client.execute(request);

        int statusCode = response.getStatusLine().getStatusCode();


        if (statusCode == 200) {
            HttpEntity entity = response.getEntity();
            result = EntityUtils.toString(entity);
        } else {
            result = "error";
        }

        return result;

    }
}
