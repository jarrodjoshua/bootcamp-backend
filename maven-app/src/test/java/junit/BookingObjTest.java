package junit;

import static org.junit.Assert.*;

import org.junit.Test;

import travelplusplus.maven_app.model.Bookings;
import travelplusplus.maven_app.model.User;

public class BookingObjTest {
	
	Bookings b = new Bookings();

	@Test
	public void test() {
		
		String s = "abc";
		Integer i = new Integer(1);
		
		b.setBookingid(new Integer(1));
		assertEquals(b.getBookingid(),i);
		
		User u = new User();
		b.setUser(u);
		assertEquals(b.getUser(),u);
		
		b.setFlightTimeDep("abc");
		assertEquals("abc",s);
		assertEquals(b.getFlightTimeDep(),s);
		
		b.setFlightTimeArrival("abc");
		assertEquals("abc",s);
		assertEquals(b.getFlightTimeArrival(),s);
		
		b.setFlightClass("abc");
		assertEquals("abc",s);
		assertEquals(b.getFlightClass(),s);
		
		b.setFlightNoAdults("abc");
		assertEquals("abc",s);
		assertEquals(b.getFlightNoAdults(),s);
		
		b.setFlightNoChildren("abc");
		assertEquals("abc",s);
		assertEquals(b.getFlightNoChildren(),s);
		
		b.setFlightFromAirport("abc");
		assertEquals("abc",s);
		assertEquals(b.getFlightFromAirport(),s);
		
		b.setFlightToAirport("abc");
		assertEquals("abc",s);
		assertEquals(b.getFlightToAirport(),s);
		
		b.setFlightAircraft("abc");
		assertEquals("abc",s);
		assertEquals(b.getFlightAircraft(),s);
		
		
		b.setHotelName("abc");
		assertEquals("abc",s);
		assertEquals(b.getHotelName(),s);
		
		b.setHotelRating("abc");
		assertEquals("abc",s);
		assertEquals(b.getHotelRating(),s);
		
		b.setHotelTimeCheckIn("abc");
		assertEquals("abc",s);
		assertEquals(b.getHotelTimeCheckIn(),s);
		
		b.setHoteTimeCheckOut("abc");
		assertEquals("abc",s);
		assertEquals(b.getHoteTimeCheckOut(),s);
		
		b.setHoteltNoAdults("abc");
		assertEquals("abc",s);
		assertEquals(b.getHoteltNoAdults(),s);
		
		b.setHotelNoChildren("abc");
		assertEquals("abc",s);
		assertEquals(b.getHotelNoChildren(),s);
		
		b.setHotelBedType("abc");
		assertEquals("abc",s);
		assertEquals(b.getHotelBedType(),s);
		
	}

}
