package junit;

import static org.junit.Assert.*;

import org.junit.Test;

import com.fasterxml.jackson.annotation.JsonProperty;

import travelplusplus.maven_app.apiResponses.FlightModels.FlightInboundSegment;

public class FlightInboundSegObjTest {


//    public String arrivalDateTime;
//    public Integer flightNumber;
//    public String destinationTerminal;
//    public String aircraft;
	
	FlightInboundSegment f = new FlightInboundSegment();
	
	@Test
	public void test() {
		
		String s = "abc";
		
		f.setSegmentNumber(Integer.parseInt("1"));
		Integer i = new Integer(1);
		assertEquals(f.getSegmentNumber(),i);
		
		f.setDepartureAirport("abc");
		assertEquals("abc",s);
		assertEquals(f.getDepartureAirport(),s);
		
		f.setArrivalAirport("abc");
		assertEquals("abc",s);
		assertEquals(f.getArrivalAirport(),s);
		
		f.setMarketingAirline("abc");
		assertEquals("abc",s);
		assertEquals(f.getMarketingAirline(),s);
		
		f.setDepartureDateTime("abc");
		assertEquals("abc",s);
		assertEquals(f.getDepartureDateTime(),s);
		
		f.setDestinationTerminal("abc");
		assertEquals("abc",s);
		assertEquals(f.getDestinationTerminal(),s);
		
		f.setAircraft("abc");
		assertEquals("abc",s);
		assertEquals(f.getAircraft(),s);
		
		f.setFlightNumber(Integer.parseInt("1"));
		Integer j = new Integer(1);
		assertEquals(f.getFlightNumber(),j);
		
	}

}
