package junit;

import static org.junit.Assert.*;

import java.util.List;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Test;

import travelplusplus.maven_app.apiResponses.Flight;
import travelplusplus.maven_app.apiResponses.FlightModels.FlightInboundSegment;
import travelplusplus.maven_app.apiResponses.FlightModels.FlightOutboundSegment;

public class FlightObjTest {
	
//    public Response.Status status;
//
//    public List<FlightOutboundSegment> flightOutboundSegmentList;
//    public List<FlightInboundSegment> flightInboundSegmentList;
//
	
	Flight f = new Flight();

	@Test
	public void test() {
		String t = "123";
		f.setFareCurrency("123");
		assertEquals("123", t);	
		assertEquals(f.getFareCurrency(), t);
		
		f.setFarePricePerAdult("123");
		assertEquals("123",t);
		assertEquals(f.getFarePricePerAdult(),t);
		
		f.setFarePricePerAdultTax("123");
		assertEquals("123",t);
		assertEquals(f.getFarePricePerAdultTax(),t);
		
		f.setFarePricePerChild("123");
		assertEquals("123",t);
		assertEquals(f.getFarePricePerChild(),t);
		
		f.setFarePricePerChildTax("123");
		assertEquals("123",t);
		assertEquals(f.getFarePricePerChildTax(),t);
		
		f.setFareTotalPrice("123");
		assertEquals("123",t);
		assertEquals(f.getFareTotalPrice(),t);
		
		f.setFromAirport("123");
		assertEquals("123",t);
		assertEquals(f.getFarePricePerAdultTax(),t);
		
		f.setInboundSeatsRemaining(1);
		assertEquals("123",t);
		assertEquals(f.getInboundSeatsRemaining(),new Integer(1));
		
		f.setOutboundAirlineName("123");
		assertEquals("123",t);
		assertEquals(f.getOutboundAirlineName(),t);
		
		f.setTravelClass("123");
		assertEquals("123",t);
		assertEquals(f.getTravelClass(),t);
		
		f.setToAirport("123");
		assertEquals("123",t);
		assertEquals(f.getToAirport(),t);
		
		f.setReturnFlightTimeDuration("123");
		assertEquals("123",t);
		assertEquals(f.getReturnFlightTimeDuration(),t);
		
		f.setOutboundFromToAirport("123");
		assertEquals("123",t);
		assertEquals(f.getOutboundFromToAirport(),t);
		
		f.setOutboundSeatsRemaining(12);
		assertEquals("123",t);
		assertEquals(f.getOutboundSeatsRemaining(),new Integer(12));
		
		f.setReturnFlightTimeDuration("123");
		assertEquals("123",t);
		assertEquals(f.getReturnFlightTimeDuration(),t);
		
		f.setOutboundFlightTimeDuration("123");
		assertEquals("123",t);
		assertEquals(f.getOutboundFlightTimeDuration(),t);
		
		f.setResultID(123);
		assertEquals("123",t);
		assertEquals(f.getResultID(),123);
		
		f.setFromAirport("123");
		assertEquals("123",t);
		assertEquals(f.getFromAirport(),t);
		
		f.setOutboundFlightTime("123");
		assertEquals("123",t);
		assertEquals(f.getOutboundFlightTime(),t);
		
		f.setReturnFlightTime("123");
		assertEquals("123",t);
		assertEquals(f.getReturnFlightTime(),t);
		
		f.setReturnFromToAirport("123");
		assertEquals("123",t);
		assertEquals(f.getReturnFromToAirport(),t);
		
		f.setReturnAirlineName("123");
		assertEquals("123",t);
		assertEquals(f.getReturnAirlineName(),t);
		
		f.setStatus(null);
		assertEquals("123",t);
		assertEquals(f.getStatus(),null);
		
		f.setFlightInboundSegmentList(null);
		assertEquals("123",t);
		assertEquals(f.getFlightInboundSegmentList(),null);
		
		f.setFlightOutboundSegmentList(null);
		assertEquals("123",t);
		assertEquals(f.getFlightOutboundSegmentList(),null);
		
	}

}
