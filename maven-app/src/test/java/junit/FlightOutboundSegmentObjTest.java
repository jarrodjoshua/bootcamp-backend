package junit;

import static org.junit.Assert.*;

import org.junit.Test;

import com.fasterxml.jackson.annotation.JsonProperty;

import travelplusplus.maven_app.apiResponses.FlightModels.FlightOutboundSegment;

public class FlightOutboundSegmentObjTest {
	
//	@JsonProperty
//    public Integer segmentNumber;
//    @JsonProperty
//    public String departureAirport;
//    @JsonProperty
//    public String arrivalAirport;
//    @JsonProperty
//    public String marketingAirline;
//    @JsonProperty
//    public String departureDateTime;
//    @JsonProperty
//    public String arrivalDateTime;
//    @JsonProperty
//    public Integer flightNumber;
//    @JsonProperty
//    public String destinationTerminal;
//    @JsonProperty
//    public String aircraft;
//    @JsonProperty
//    public String departureAirportName;
//    @JsonProperty
//    public String arrivalAirportName;

	FlightOutboundSegment f = new FlightOutboundSegment();
	
	@Test
	public void test() {
		
		String s = "abc";
		
		f.setSegmentNumber(Integer.parseInt("1"));
		Integer i = new Integer(1);
		assertEquals(f.getSegmentNumber(),i);
		
		f.setDepartureAirport("abc");
		assertEquals("abc",s);
		assertEquals(f.getDepartureAirport(),s);
		
		f.setArrivalAirport("abc");
		assertEquals("abc",s);
		assertEquals(f.getArrivalAirport(),s);
		
		f.setMarketingAirline("abc");
		assertEquals("abc",s);
		assertEquals(f.getMarketingAirline(),s);
		
		f.setDepartureDateTime("abc");
		assertEquals("abc",s);
		assertEquals(f.getDepartureDateTime(),s);
		
		f.setDestinationTerminal("abc");
		assertEquals("abc",s);
		assertEquals(f.getDestinationTerminal(),s);
		
		f.setAircraft("abc");
		assertEquals("abc",s);
		assertEquals(f.getAircraft(),s);
		
		f.setFlightNumber(Integer.parseInt("1"));
		Integer j = new Integer(1);
		assertEquals(f.getFlightNumber(),j);
		
	}

}
