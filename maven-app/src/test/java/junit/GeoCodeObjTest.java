package junit;

import static org.junit.Assert.*;

import org.junit.Test;

import travelplusplus.maven_app.apiResponses.GeoCode;

public class GeoCodeObjTest {
	
	GeoCode g = new GeoCode();
	
//	private String localName;
//  private String administrativeAreaLevel2;
//    private String getAdministrativeAreaLevel1;
//    private String country;
//
//    private String latitude;
//    private String longitude;


	@Test
	public void test() {
		
		String t = "abc";
		
		g.setAdministrativeAreaLevel2("abc");
		assertEquals("abc", t);
		assertEquals(g.getAdministrativeAreaLevel2(),t);
		
		g.setCountry("abc");
		assertEquals("abc", t);
		assertEquals(g.getCountry(),t);
		
		g.setGetAdministrativeAreaLevel1("abc");
		assertEquals("abc", t);
		assertEquals(g.getGetAdministrativeAreaLevel1(),t);
		
		g.setLatitude("abc");;
		assertEquals("abc", t);
		assertEquals(g.getLatitude(),t);
		
		g.setLongitude("abc");;
		assertEquals("abc", t);
		assertEquals(g.getLongitude(),t);
		
		g.setLocalName("abc");
		assertEquals("abc", t);
		assertEquals(g.getLocalName(),t);
		
		
	}

}
