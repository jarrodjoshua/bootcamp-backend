package junit;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import travelplusplus.maven_app.apiResponses.Hotel;

public class HotelObjTest {

//    private List<String> amenities;
//    private List<String> images;
	
	Hotel h = new Hotel();

	@Test
	public void test() {
		
		String t = "abc";
		
		h.setHotelName("abc");
		assertEquals("abc",t);
		assertEquals(h.getHotelName(),t);
		
		h.setAddress("abc");
		assertEquals("abc",t);
		assertEquals(h.getAddress(),t);
		
		h.setCountry("abc");
		assertEquals("abc",t);
		assertEquals(h.getCountry(),t);
		
		h.setCity("abc");
		assertEquals("abc",t);
		assertEquals(h.getCity(),t);
		
		h.setMarketingText("abc");
		assertEquals("abc",t);
		assertEquals(h.getMarketingText(),t);
		
		h.setBedType("abc");
		assertEquals("abc",t);
		assertEquals(h.getBedType(),t);
		
		h.setNumberOfBeds("abc");
		assertEquals("abc",t);
		assertEquals(h.getNumberOfBeds(),t);
		
		h.setLongitude("abc");
		assertEquals("abc",t);
		assertEquals(h.getLongitude(),t);
		
		h.setLatitude("abc");
		assertEquals("abc",t);
		assertEquals(h.getLatitude(),t);
		
		h.setPhoneNo("abc");
		assertEquals("abc",t);
		assertEquals(h.getPhoneNo(),t);
		
		h.setTotalPrice("abc");
		assertEquals("abc",t);
		assertEquals(h.getTotalPrice(),t);
		
		h.setAmenities(null);
		assertEquals("abc",t);
		assertEquals(h.getAmenities(),null);
		
		h.setImages(null);
		assertEquals("abc",t);
		assertEquals(h.getImages(),null);
		
	}

}
