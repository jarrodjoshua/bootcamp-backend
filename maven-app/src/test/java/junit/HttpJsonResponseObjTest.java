package junit;

import static org.junit.Assert.*;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Test;

import travelplusplus.maven_app.errorResponses.HTTPJSONResponses;

public class HttpJsonResponseObjTest {
	
//	private String message;
//  private Response.Status status;

	HTTPJSONResponses h = new HTTPJSONResponses();
	
	@Test
	public void test() {
		
		String s = "abc";
		
		HTTPJSONResponses notFound = new HTTPJSONResponses();
		Response r;
		
		h.setMessage("abc");
		assertEquals(s,"abc");
		assertEquals(h.getMessage(),s);
		
		h.setStatus(Response.Status.NOT_FOUND);
		assertEquals(h.getStatus(),Response.Status.NOT_FOUND);
		
		notFound = new HTTPJSONResponses();
        notFound.setMessage(s);
        notFound.setStatus(Response.Status.OK);
        r = Response.ok(notFound).type(MediaType.APPLICATION_JSON).build();
		assertEquals(HTTPJSONResponses.loginSuccess(s).getStatus(),r.getStatus());
		
		notFound.setStatus(Response.Status.NOT_FOUND);
		notFound = new HTTPJSONResponses();
        notFound.setMessage(s);
		r = Response.ok(notFound).type(MediaType.APPLICATION_JSON).build();
		assertEquals(HTTPJSONResponses.notFoundResponse(s).getStatus(),r.getStatus());
		
		notFound = new HTTPJSONResponses();
        notFound.setMessage(s);
		notFound.setStatus(Response.Status.UNAUTHORIZED);
		r = Response.ok(notFound).type(MediaType.APPLICATION_JSON).build();
		assertEquals(HTTPJSONResponses.unauthorized(s).getStatus(),r.getStatus());
		
	}

}
