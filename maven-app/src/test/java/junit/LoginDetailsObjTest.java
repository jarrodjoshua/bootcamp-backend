package junit;

import static org.junit.Assert.*;

import org.junit.Test;

import travelplusplus.maven_app.details.LoginDetails;

public class LoginDetailsObjTest {
	
	LoginDetails l = new LoginDetails();

	@Test
	public void test() {
		
		String s = "abc";
		
		l.setPassword("abc");
		assertEquals(s,"abc");
		assertEquals(l.getPassword(),s);
		
		l.setUsername("abc");
		assertEquals("abc",s);
		assertEquals(l.getUsername(),s);
		
	}

}
