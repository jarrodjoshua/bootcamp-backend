package junit;

import travelplusplus.maven_app.apiResponses.TvProgram;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MockitoExample {

    private static List<TvProgram> mockTvProgramList = mock(List.class);
    private static TvProgram loveIsland;
    private static TvProgram suits;

    @BeforeClass
    public static void setUp() {

        loveIsland = new TvProgram("c73294ba-2847-4058-9648-e1c9e0ab8f66",
                "Love Island",
                "eries",
                "http://images.metadata.sky.com/pd-image/\"c73294ba-2847-4058-9648-e1c9e0ab8f66",
                "Majorca", 3.5);

        suits = new TvProgram("49e27f96-f64a-41cd-a032-864054589d18",
                "Suits",
                "series",
                "http://images.metadata.sky.com/pd-image/\"49e27f96-f64a-41cd-a032-864054589d18",
                "New York", 4.2);

        mockTvProgramList.add(loveIsland);
        mockTvProgramList.add(suits);

        when(mockTvProgramList.get(0)).thenReturn(loveIsland);
        when(mockTvProgramList.get(1)).thenReturn(suits);

    }

    @Test
    public void testGetSeries() {
        assertEquals(loveIsland, mockTvProgramList.get(0));
        assertEquals(suits, mockTvProgramList.get(1));
    }

   
}
