package junit;

import static org.junit.Assert.*;

import org.junit.Test;

import travelplusplus.maven_app.model.Location;
import travelplusplus.maven_app.model.Trip;
import travelplusplus.maven_app.model.User;


public class TripObjTest {
	
//	private Integer tripid;
//
//	private User user;
//
//	private Location locationid;
//
//	private String dateDeparture;
//	private String dateArrival;
//	private String review;
//	private String imageURL;

	Trip t = new Trip();

	@Test
	public void test() {
		
		Integer i = new Integer(1);
		String s = "abc";
		
		t.setDateArrival("abc");
		assertEquals("abc",s);
		assertEquals(t.getDateArrival(),s);
		
		t.setDateDeparture("abc");
		assertEquals("abc",s);
		assertEquals(t.getDateDeparture(),s);
		
		t.setImageURL("abc");
		assertEquals("abc",s);
		assertEquals(t.getImageURL(),s);
		
		t.setReview("abc");
		assertEquals("abc",s);
		assertEquals(t.getReview(),s);
		
		Location l = new Location();
		
		t.setLocationid(l);
		assertEquals(t.getLocationid(),l);
		
		User u = new User();
		
		t.setUser(u);
		assertEquals(u,t.getUser());
		
		t.setTripid(new Integer(1));
		assertEquals(t.getTripid(),i);
		
	}

}
