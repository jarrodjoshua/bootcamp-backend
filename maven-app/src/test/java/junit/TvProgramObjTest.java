package junit;

import static org.junit.Assert.*;

import org.junit.Test;

import travelplusplus.maven_app.apiResponses.TvProgram;

public class TvProgramObjTest {

//	    private String city;
//	    private String lat;
//	    private String lon;
//	    private double rating;
	
	TvProgram tv = new TvProgram(null, null, null, null, null, 0);
	
	@Test
	public void test() {
		
		String t = "abc";
		
		tv.setTitle("abc");
		assertEquals("abc",t);
		assertEquals(tv.getTitle(),t);
		
		tv.setId("abc");
		assertEquals("abc",t);
		assertEquals(tv.getId(),t);
		
		tv.setType("abc");
		assertEquals("abc",t);
		assertEquals(tv.getType(),t);
		
		tv.setImageUrl("abc");
		assertEquals("abc",t);
		assertEquals(tv.getImageUrl(),t);
		
		tv.setCity("abc");
		assertEquals("abc",t);
		assertEquals(tv.getCity(),t);
		
		tv.setLat("abc");
		assertEquals("abc",t);
		assertEquals(tv.getLat(),t);
		
		tv.setLon("abc");
		assertEquals("abc",t);
		assertEquals(tv.getLon(),t);
		
//		tv.setRating(1.9);
//		double d = 1.8;
//		assertEquals(tv.getRating(),d);
		
	}

}
