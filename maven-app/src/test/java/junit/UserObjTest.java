package junit;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import travelplusplus.maven_app.model.Bookings;
import travelplusplus.maven_app.model.Trip;
import travelplusplus.maven_app.model.User;

public class UserObjTest {
	
//	@Id
//	private String username;
//	private String first_name;
//	private String last_name;
//	private String password;
//	private Integer socialMediaEnabled;
//	private String rank;
//	private String role;


//	@OneToMany(targetEntity=Trip.class, mappedBy="user")
//	private List<Trip> trips;
//
//
//	@OneToMany(targetEntity=Bookings.class, mappedBy="user")
//	private List<Bookings> bookings;

	User u = new User();

	@Test
	public void test() {
		
		String s = "abc";
		Integer i = new Integer(1);
		List<Trip> l = null;
		
		u.setFirst_name("abc");
		assertEquals("abc",s);
		assertEquals(u.getFirst_name(),s);
		
		u.setLast_name("abc");
		assertEquals("abc",s);
		assertEquals(u.getLast_name(),s);
		
		u.setUsername("abc");
		assertEquals("abc",s);
		assertEquals(u.getFirst_name(),s);
		
		u.setPassword("abc");
		assertEquals("abc",s);
		assertEquals(u.getFirst_name(),s);
		
		u.setSocialMediaEnabled(new Integer(1));
		assertEquals("abc",s);
		assertEquals(u.getSocialMediaEnabled(),i);
		
		u.setRank("abc");
		assertEquals("abc",s);
		assertEquals(u.getRank(),s);
		
		u.setRole("abc");
		assertEquals("abc",s);
		assertEquals(u.getRole(),s);
		
		u.setTrips(null);
		assertEquals(u.getTrips(),null);
		
		u.setBookings(null);
		assertEquals(u.getBookings(),null);
		
	}

}
