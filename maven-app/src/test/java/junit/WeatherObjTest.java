package junit;

import static org.junit.Assert.*;

import org.junit.Test;

import travelplusplus.maven_app.apiResponses.Weather;

public class WeatherObjTest {
	
//	private String weather;
//    private String weatherDescription;
//    private int temperature;
//    private String countryCode;
	
	Weather w = new Weather();

	@Test
	public void test() {
		
		String s = "abc";
		
		w.setCountryCode("abc");
		assertEquals("abc",s);
		assertEquals(w.getCountryCode(),s);
		
		w.setWeather("abc");
		assertEquals("abc",s);
		assertEquals(w.getWeather(),s);
		
		w.setWeatherDescription("abc");
		assertEquals("abc",s);
		assertEquals(w.getWeatherDescription(),s);
		
//		w.setTemperature(123);
//		double d = 123.0;
//		assertEquals(w.getTemperature(),d);
		
	}

}
